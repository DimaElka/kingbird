//
//  MainVC.swift
//  KingBird
//
//  Created by Rogov Dmitiy on 07.12.16.
//  Copyright © 2016 Rogov Dmitry. All rights reserved.
//

import UIKit
import GoogleMaps

class MainVC: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var distanceLabel: UILabel!
    
    var locationManager = CLLocationManager()
    
    var currentLocationIsSet = false
    var myLat: Double = 0.0
    var myLong: Double = 0.0
    
    let destLat: Double = 55.778914190059794
    let destLong: Double = 37.59541869163513
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.distanceLabel.text = "Идет подсчет расстояния..."
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
    }
    
    // MARK: Location Permissions
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse: initMap()
        case .notDetermined: locationManager.requestWhenInUseAuthorization()
        case .denied, .restricted: showNeedRightsAlert()
        default: break
        }
    }
    
    // MARK: Map
    
    func initMap() {
        
        // Finding current location
        
        locationManager.startUpdatingLocation()
        
        if let location: CLLocation = locationManager.location {
            let target: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let camera: GMSCameraPosition = GMSCameraPosition(target: target, zoom: 12, bearing: 0, viewingAngle: 0)
            
            self.myLong = location.coordinate.longitude
            self.myLat = location.coordinate.latitude
            self.currentLocationIsSet = true
            
            if let map = googleMapView {
                
                let myPositionMarker = GMSMarker(position: target)
                myPositionMarker.icon = UIImage(named: "Pin")
                myPositionMarker.map = googleMapView
                
                map.camera = camera
                map.delegate = self
            }
            
            // Adding spot with lat 55.778914190059794, lon 37.59541869163513
            

            let spotPosition = CLLocationCoordinate2DMake(destLat, destLong)
            let spotMarker = GMSMarker(position: spotPosition)
            spotMarker.icon = UIImage(named: "Info")
            spotMarker.map = googleMapView
            
            // Counting distance
            let fromCoordinate = CLLocation(latitude: target.latitude, longitude: target.longitude)
            let toCoordinate = CLLocation(latitude: destLat, longitude: destLong)
            let meters = countDistance(from: fromCoordinate, to: toCoordinate)
            
            if meters < 1000 {
                self.distanceLabel.text = "Расстояние до точки: \(meters) м."
            } else {
                let kilometers = meters / 1000
                self.distanceLabel.text = "Расстояние до точки: \(kilometers) км."
            }
        }
    }
    
    
    @IBAction func showNavigation(_ sender: UIButton) {
        
        if currentLocationIsSet {
            
            let alertController = UIAlertController(title: "Проложить маршрут", message: "Выберете программу для прокладки маршрута до точки", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)

            let openYandexAction = UIAlertAction(title: "Яндекс.карты", style: .default, handler: { action in
                
                self.openUrl(appName: "yandexmaps://", appURL: "yandexmaps://maps.yandex.ru/?rtext=\(self.myLat),\(self.myLong)~\(self.destLat),\(self.destLong)&rtt=auto", safariUrl: "https://maps.yandex.ru/?rtext=\(self.myLat),\(self.myLong)~\(self.destLat),\(self.destLong)&rtt=auto")
                
            })
            
            let openYandexNavigation = UIAlertAction(title: "Яндекс.навигатор", style: .default, handler: { action in
                
                self.openUrl(appName: "yandexnavi://", appURL: "yandexnavi://build_route_on_map?lat_from=\(self.myLat)&lon_from=\(self.myLong)&lat_to=\(self.destLat)&lon_to=\(self.destLong)", safariUrl: "https://maps.yandex.ru/?rtext=\(self.myLat),\(self.myLong)~\(self.destLat),\(self.destLong)&rtt=auto")
                
            })
            
            let openGoogleMaps = UIAlertAction(title: "Google Maps", style: .default, handler: { action in
                
                self.openUrl(appName: "comgooglemaps://", appURL: "comgooglemaps://?saddr=\(self.myLat),\(self.myLong)&daddr=\(self.destLat),\(self.destLong)", safariUrl: "https://www.google.ru/maps/dir/\(self.myLat),\(self.myLong)/\(self.destLat),\(self.destLong)/")
                
            })
            
            alertController.addAction(cancelAction)
            alertController.addAction(openYandexAction)
            alertController.addAction(openYandexNavigation)
            alertController.addAction(openGoogleMaps)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            showAlert(title: "Ошибка", message: "Программе не удалось определить ваше местоположение")
            
        }
        
    }
    
    // MARK: Misc
    
    func showNeedRightsAlert() {
        
        let alertController = UIAlertController(title: "Доступ к вашей геопозиции запрещен", message: "Для того, чтобы приложение смогло определить вашу георозицию ему необходимо разрешить это в настройках", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        let openAction = UIAlertAction(title: "Открыть настройки", style: .default, handler: { action in
            let url = NSURL(string:UIApplicationOpenSettingsURLString)
            UIApplication.shared.openURL(url! as URL)
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func countDistance(from: CLLocation, to: CLLocation) -> Int {
        
        let distanceMeters = from.distance(from: to)
        let round = Int(distanceMeters)
        return round
        
    }
    
    
    func showAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "ОК", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openUrl(appName: String, appURL: String, safariUrl: String) {
        if (UIApplication.shared.canOpenURL(NSURL(string: appName)! as URL)) {
            UIApplication.shared.openURL(NSURL(string: appURL)! as URL)
        } else {
            print("Can't use app, using Safari")
            UIApplication.shared.openURL(URL(string: safariUrl)!)
        }
    }
    
    
}
